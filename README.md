# PowerGenerationUi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.4.

This project provides a user interface for visualizing and filtering the annual net generation data of power plants in the United States. 

## Deployment
The app is deployed on GitHub Pages. You can access it using this [URL](https://nermeen-mattar.github.io/power-generation-ui/). Please note that the initial render will take some time as I have enabled the user to view all plants across states. Subsequent requests will be faster when utilizing the available filters

## Demo
Take a look at this short [demo](https://drive.google.com/file/d/1bSnlSSHqibbvOaBS-v5VRbCIBmt6ZuMA/view?usp=sharing)

## Handling Data Inconsistencies:
I have made efforts to handle inconsistencies in the data. However, due to the sheer volume of data, it is possible that some values are incorrect or missing. In such cases, you may encounter instances where the percentage value is not available due to an issue in the *Generator annual net generation (MWh)* for certain power plants. It does not reflect a functionality issue but rather indicates that the data was not provided or is incorrect. 

## Functionality
The app offers users the flexibility to filter power plants based on their preferences. Here are the supported features:
- View all power plants in the US.
- Narrow down the results by selecting the top plants in a specific state.
- Filter by the top plants across all states in the US.

## Development
To run the project locally, follow these steps:

- Clone the repository.
- Navigate to the project directory.
- Run npm install to install the dependencies.
- Run `ng serve` for a development server. The app will be served at `http://localhost:4200/`. Any changes made to the source files will trigger an automatic reload.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
